terraform {
  required_version = "1.0.4"

  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.6.10"
    }
  }
}

provider "libvirt" {
#  uri = "qemu+ssh://putchar@dedibox-1/system"
  # hack until the provider is fixed
  uri = "qemu+unix:///session?socket=/tmp/libvirt-sock"
}
