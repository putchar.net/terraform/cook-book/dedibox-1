#!/usr/bin/env sh

set -xe

cleanup() {
    for pid in $(pgrep ssh \-fnNT); do kill -3 "$pid"; done
    rm $1
}

TMPDIR="/tmp"

libvirt_sock="$TMPDIR/libvirt-sock"
ssh -fnNT -L "$libvirt_sock":/var/run/libvirt/libvirt-sock -i ~/.ssh/id_rsa putchar@dedibox-1
trap "cleanup $libvirt_sock" ERR EXIT
export LIBVIRT_DEFAULT_URI="qemu+unix:///session?socket=$libvirt_sock"

#Verify Connection
virsh -c "$LIBVIRT_DEFAULT_URI" sysinfo >/dev/null

#Use terraform
#for cmd in init plan apply; do
for cmd in init plan; do
    terraform "$cmd"
done
trap "terraform destroy" EXIT ERR

echo "Press Return to Quit & Cleanup..."
read
