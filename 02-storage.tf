module "storage_root" {

  source       = "git::git@gitlab.com:putchar.net/terraform/modules/libvirt_pool.git?ref=1.0.0"
  storage_name = "pool_root"
  storage_type = "dir"
  storage_path = var.storage_root_path
}

output "storage_root_name" {
  value = module.storage_root.storage_pool_name
}

module "storage_data" {

  source       = "git::git@gitlab.com:putchar.net/terraform/modules/libvirt_pool.git?ref=1.0.0"
  storage_name = "pool_data"
  storage_type = "dir"
  storage_path = var.storage_data_path
}

output "storage_data_name" {
  value = module.storage_data.storage_pool_name
}

module "storage_iso" {
  source       = "git::git@gitlab.com:putchar.net/terraform/modules/libvirt_pool.git?ref=1.0.0"
  storage_name = "pool_iso"
  storage_type = "dir"
  storage_path = var.storage_iso_path
}

output "storage_iso_name" {
  value = module.storage_iso.storage_pool_name
}
