## 02-storage.tf
variable "storage_root_path" {
  type = string
}

variable "storage_data_path" {
  type = string
}

variable "storage_iso_path" {
  type = string
}


variable "gitlab_api_token" {
  type = string
}
